import hashlib, time, binascii
import system, mouse, input_mikrofon
from hashlib import sha256

key_size = 256
numeral_system = "HEX"


def generator(key_size,numeral_system):
    entropy1 = system.system()
    entropy2 = mouse.mouse()
    entropy3 = input_mikrofon.mikrofon()

    if key_size == 56:
        entropy = sha256((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:14]
    elif key_size == 168:
        entropy = sha256((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:42]
    elif key_size == 128:
        entropy = sha256((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:32]
    elif key_size == 192:
        entropy = sha256((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:48]
    else:
        entropy = sha256((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:64]

    if numeral_system == "BIN":
        #key = format(int(key,16), '0%db'%key_size)         #binární 
        key = bin(int(entropy,16))[2:].zfill(key_size)
        return str(key),entropy                             #vracime klic i jeho hex typ
    elif numeral_system == "DEC":
        key = int(entropy,16)                               #decimální
        return (str(key),entropy)
    else:
        return entropy,entropy
        
        
