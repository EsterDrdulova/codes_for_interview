import psutil
import re, os
from hashlib import sha256

#funkce pro zisk systémových dat
def system():
    mem_disk_net = (str(psutil.virtual_memory()+ psutil.disk_io_counters() +
                        psutil.net_io_counters()))
    numbers = re.findall(r'\d+', mem_disk_net)

    #vybáráme volnou a použitou RAM, počet čtení a zápisů na disku,
    #počet odeslaných a přijatých bytů
    num = str(numbers[1])+ str(numbers[4])+ str(numbers[6])+ str(numbers[7]) +str(numbers[13])+str(numbers[14])
    sh = str(sha256(num.encode()).hexdigest())

    """ #zápis získaných dat do souboru
    if os.path.exists('mem_disk_net.txt'):
        os.remove('mem_disk_net.txt')
    with open('mem_disk_net.txt', 'w', encoding='utf-8') as f:
        f.write(str(sh))
        f.close()
            #zapíše volnou a použitou RAM, počet četní a zápisů na disku,
            #počet odeslaných a přijatých bytů
    """
    
    return sh
