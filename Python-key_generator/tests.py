from collections import Counter


def convert_hex_binary(hex_text): #prevod z hex. soustavy na binarni
    binary_result = ''
    for character in hex_text:
        binary_result += "{0:04b}".format(int(character, 16))
    return binary_result


def monobit_test(bin_string): #test cetnosti jednicek
    number_of_ones = 0
    for bit in bin_string: #pokud v bin. stringu je 1, pocet number_of_ones se zvysi 
        if bit == '1':
            number_of_ones += 1
    if (number_of_ones > 9654) and (number_of_ones < 10346): #pokud je pocet v rozmezi 9654 a 10346
        return True
    else:
        return False


def poker_test(bin_string): #Posloupnost 20 000 bitu rozdelena na 5000 ctyrbit. useku, reprezentuji i = 0 .. 15. Pocet useku s h. i si oznacme jako f(i).
    contiguous_segments = [bin_string[i:i+4] for i in range(0, len(bin_string), 4)]
    count = Counter(contiguous_segments)

    f_i = 0
    for string, amount in count.items():
        f_i += amount*amount
    x = ((16/5000) * f_i) - 5000 #vypocet x

    if (x > 1.03) and (x < 57.4):
        return True
    else:
        return False


def runs_test(bin_string): #run je usek posloupnosti slozen z samych 0(gap) nebo 1(blok) 
    count = 1
    repetitions = []
    counter_0 = [0, 0, 0, 0, 0, 0] #pole pro pocet gapu (1,2,3,4,5,6)
    counter_1 = [0, 0, 0, 0, 0, 0] #pole pro pocet bloku (1,2,3,4,5,6)
    if len(bin_string) > 1:
        for i in range(1, len(bin_string)):
            if bin_string[i - 1] == bin_string[i]:
                count += 1 #kolik se shoduje vedle sebe 0 nebo 1
            else:
                repetitions.append([bin_string[i - 1], count])
                count = 1
        repetitions.append([bin_string[i], count])

    for rep in repetitions: #kontrola poctu gapu a bloku delky 1 az 6
        if rep[0] == '0':
            if rep[1] >= 6:
                counter_0[5] += 1
            else:
                counter_0[rep[1]-1] += 1
        if rep[0] == '1':
            if rep[1] >= 6:
                counter_1[5] += 1
            else:
                counter_1[rep[1]-1] += 1

    zero_ok = False
    one_ok = False
    #kontrola, jestli pocet gapu odpovida intervalu run testu
    if (counter_0[0] > 2267) and (counter_0[0] < 2733) and (counter_0[1] > 1079) and (counter_0[1] < 1421) and \
        (counter_0[2] > 502) and (counter_0[2] < 748) and (counter_0[3] > 223) and (counter_0[3] < 402) and \
        (counter_0[4] > 90) and (counter_0[4] < 223) and (counter_0[5] > 90) and (counter_0[5] < 223):
        zero_ok = True

    #kontrola, jestli pocet bloku odpovida intervalu run testu
    if (counter_1[0] > 2267) and (counter_1[0] < 2733) and (counter_1[1] > 1079) and (counter_1[1] < 1421) and \
        (counter_1[2] > 502) and (counter_1[2] < 748) and (counter_1[3] > 223) and (counter_1[3] < 402) and \
        (counter_1[4] > 90) and (counter_1[4] < 223) and (counter_1[5] > 90) and (counter_1[5] < 223):
        one_ok = True

    if zero_ok and one_ok:
        return True
    else:
        return False


def long_run(bin_string): #test delky bin retezce (kolik za sebou 0 nebo 1)
    count = 1
    if len(bin_string) > 1:
        for i in range(1, len(bin_string)):
            if bin_string[i - 1] == bin_string[i]:
                count += 1
            else:
                if count >= 34: #pokud pocet prevysi 34 => false
                    return False
                count = 1
    return True




