import pyautogui
import os
from hashlib import sha256

#funkce pro zisk polohy myši
def mouse():
    x,y = pyautogui.position()          #výpis pozice myši
    mouse = sha256(str(x+y).encode()).hexdigest()

    """ #zápis získaných dat do souboru
    if os.path.exists('mouse.txt'):
        os.remove('mouse.txt')
    with open('mouse.txt', 'w', encoding='utf-8') as f:
        f.write(mouse)
        f.write('\n')
        f.close()
    """
    
    return mouse
