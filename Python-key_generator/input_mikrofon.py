import pyaudio
import os
from hashlib import sha256

#funkce pro zisk dat z mikrofonu
def mikrofon():
    CHUNK = 1024                    #počet snímků, na které je signál rozdělen
    FORMAT = pyaudio.paInt16        #velikost každého vzorku je 2 byty
    CHANNELS = 2                    #každý snímek bude mít 2 vzorky
    RATE = 44100                    #množství vzorků zaznamenaných za sekundu
    RECORD_SECONDS = 0.025

    p = pyaudio.PyAudio()

    #nahrávání záznamu
    stream = p.open(format=FORMAT, channels= CHANNELS, rate=RATE, input=True,
                frames_per_buffer=CHUNK)
    print("* recording")

    frames=[]                       #uložení záznamu
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)
    print("* done recording")

    stream.stop_stream()        #ukončení záznamu
    stream.close()
    p.terminate()
    
    cnt = (b''.join(frames))
    audio = sha256(str(hash(int.from_bytes(cnt, byteorder='big'))).encode()).hexdigest()

    """   #uložení získaných dat do souboru
    if os.path.exists('audio.txt'):
        os.remove('audio.txt')
    with open('audio.txt', 'w', encoding='utf-8') as f:       #vytvoření text file se záznamem
        f.write(audio)
        f.close()
    """
    
    return audio
