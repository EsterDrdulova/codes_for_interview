#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <iostream>
#include <stdio.h>

#define _CRT_SECURE_NO_WARNINGS
//#define getch() _getch()

//do headru

#define SIZE 15

//do headru


struct game
{
	char name[SIZE];
	int wins;
	struct game* dalsi;
};			

struct game* prvni = NULL; 

void addGame(char* name, int wins, struct game** uk_prvni) // pridani hry
{
	struct game* newGame; // ukazatel pro nove hry
	struct game* actGame; // ukazatel na aktualni hru

	// alokace dynamicke promenne
	newGame = (struct game*)malloc(sizeof(struct game));

	strcpy_s(newGame->name, SIZE, name);		// naplneni struktury
	newGame->wins = wins;
	newGame->dalsi = NULL;

	if (*uk_prvni == NULL) // linearni seznam je prazdny
	{
		*uk_prvni = newGame;
		return;
	}
	else if ((newGame->wins > (*uk_prvni)->wins)) // vlozime na zacatek
	{
		newGame->dalsi = *uk_prvni;
		*uk_prvni = newGame;
		return;
	}

	actGame = *uk_prvni;
	while (actGame) // prochazeni seznamu
	{
		if (actGame->dalsi == NULL) // jsme na posledni hre
		{
			actGame->dalsi = newGame; // pridavame na konec
			return;
		}
		else if (newGame->wins > actGame->dalsi->wins)
		{
			newGame->dalsi = actGame->dalsi; // vlozime za actGame
			actGame->dalsi = newGame;
			return;
		}
		actGame = actGame->dalsi; // posun na dalsi hru
	}
}

void delGame(struct game** uk_prvni) {


}

int playerCheck(const char* file, char name[15], const char* delice)
{
	FILE* f;
	char buffer[30]; // bude slouzit pro ukladani radku z textaku
	char* token, * next_token;
	int vyhry = 0;
	int citac = 0;
	int ok = 0;

	fopen_s(&f, file, "r");

	while (!feof(f))
	{
		if (fscanf_s(f, "%s", buffer, 30) > 0)
		{
			token = strtok_s(buffer, delice, &next_token);
			while (token != NULL)
			{
				citac++;
				if ((citac % 2) == 1)
				{
					if (strcmp(name, token) == 0)
					{
						ok = 1;
					}
				}
				else
				{
					vyhry = vyhry + (ok * atoi(token));
					ok = 0;
				}
				token = strtok_s(NULL, delice, &next_token);
			}
		}
	}
	fclose(f);
	return vyhry;
}

void writePlayer(const char* file1, const char* file2, char name1[15], char name2[15], int wins1, int wins2)
{
	FILE* f;
	FILE* g;
	char buffer[30]; // bude slouzit pro ukladani radku z textaku
	char* token, * next_token;
	int citac = 0;
	int ok1 = 0;
	int ok2 = 0;
	int ok1counter = 0;
	int ok2counter = 0;
	char cislo[10];
	fopen_s(&f, file1, "r");
	fopen_s(&g, file2, "w");

	while (!feof(f)) // dokud neni konec souboru 1
	{
		if (fscanf_s(f, "%s", buffer, 30) > 0) // dokud je co skenovat ze souboru
		{
			token = strtok_s(buffer, ",\n", &next_token);
			while (token != NULL)
			{
				citac++;
				if ((citac % 2) == 1)
				{
					if (strcmp(name1, token) == 0)				//kontrola, jestli name1 je stejne jako token - kdyz ano = 0
					{
						ok1 = 1;
					}
					else if (strcmp(name2, token) == 0)
					{
						ok2 = 1;
					}
					fprintf(g, token);
					fprintf(g, ",");
				}
				else
				{
					if (ok1 == 1)
					{
						sprintf_s(cislo, "%d", wins1);			//vlozi obsah wins1 do cislo
						fprintf(g, cislo);						
						ok1counter++;
					}
					else if (ok2 == 1)
					{
						sprintf_s(cislo, "%d", wins2);
						fprintf(g, cislo);
						ok2counter++;
					}
					else
					{
						fprintf(g, token);
					}
					ok1 = 0;
					ok2 = 0;
					fprintf(g, "\n");
				}
				token = strtok_s(NULL, ",\n", &next_token);
			}

		}
	}
	if (ok1counter == 0)
	{
		fprintf(g, name1);
		fprintf(g, ",");
		sprintf_s(cislo, "%d", wins1); //do stringu cislo se zapise int vyhry1
		fprintf(g, cislo);
		fprintf(g, "\n");
	}
	if (ok2counter == 0)
	{
		fprintf(g, name2);
		fprintf(g, ",");
		sprintf_s(cislo, "%d", wins2); //do stringu cislo se zapise int vyhry1
		fprintf(g, cislo);
		fprintf(g, "\n");
	}
	fclose(f);
	fclose(g);

	//kod co do souboru1 napise soubor 2
	//sem

	fopen_s(&f, file1, "w");
	fopen_s(&g, file2, "r");

	if (file1 == NULL)
	{
		fclose(g);
		exit(EXIT_FAILURE);
	}

	char a;
	while (( a = fgetc(g)) != EOF)	fputc(a, f);

	fclose(f);
	fclose(g);

}

char** createField(int size) {
	
		char** field = new char* [size];
		for (int i = 0; i < size; i++)
		{
			field[i] = new char[size];
		}
		return field;
}

char** fillField(char** field, int size) {

	for (int i = 0; i < size; i++)
	{													// vyplneni pole mezerami 
		for (int j = 0; j < size; j++)
		{
			field[i][j] = ' ';
		}
	}
	return field;
}

char** printField(char** field, int size) {


	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
	WORD saved_attributes;

	/* Save current attributes */
	GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
	saved_attributes = consoleInfo.wAttributes;

	printf("    ");
	for(int x = 1; x <= size; x++)
	{
		SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
		printf("%2d ", x);									//vezme to cislo jako 2 siroke - dvouciferne
	}

	SetConsoleTextAttribute(hConsole, saved_attributes);

	printf("\n");

	for (int i = 0; i < size; i++)
	{	
		SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN);
		printf(" %2d ", (i + 1));

		for (int j = 0; j < size; j++)
		{
			SetConsoleTextAttribute(hConsole, saved_attributes);
			printf("[%c]", field[i][j]);
		}
		printf("\n");
	}
	return field;
}

int playerMove(char** field, int size, int x, int y, char currentPlayer, int error){

	int rows, cols;	
	error = 0;

	rows = x - 1;
	cols = y - 1;

	if (rows <= size && cols <= size)
	{
		if (field[rows][cols] == ' ')
		{
			field[rows][cols] = currentPlayer;
		}
		else 
		{
			error = 1;
		}
	}
	else
	{
		error = 1;
	}
	return error;
}

char switchPlayer(char currentPlayer) {
	if (currentPlayer == 'X')
	{
		currentPlayer = 'O';
		return 'O';
	}
	else
	{
		currentPlayer = 'X';
		return 'X';
	}	
}

char randomPlayer() {
	char X = 'X';
	char O = 'O';

	srand(time(NULL));
	char random = rand() % 2;
	if (random == 0)
		return X;
	else
		return O;
}

int isRows(char** field, int size, int winreq) {

	int win = 0;
	int streak = 1;
	int max = size - 1;

	for (int i = 0; i < size; i++) //prijizdi vsechny radky
	{
		for(int j = 0; j < max; j++) //projizdi vsechny bunky na radku
		{
			if (((field[i][j]) == (field[i][j + 1])) && ((field[i][j]) != ' '))  //pokud sou bunka a bunka vedle ni stejna
			{																	// a zaroven neni prvni z nich mezera
				streak++;
			}
			else
			{
				streak = 1;
			}
			
			if (streak == winreq)
			{
				win = 1;
				i = size;
				j = (size - 1);
			}
		}
	}

	return win;
}

int isCols(char** field, int size, int winreq) {

	int win = 0;
	int streak = 1;
	int max = size - 1;

	for (int i = 0; i < size; i++) //prijizdi vsechny sloupce
	{
		for (int j = 0; j < max; j++) //projizdi vsechny bunky ve sloupci
		{
			if (((field[j][i]) == (field[j + 1][i])) && ((field[j][i]) != ' '))  //pokud sou bunka a bunka vedle ni stejna
			{																	// a zaroven neni prvni z nich mezera
				streak++;
			}
			else
			{
				streak = 1;
			}

			if (streak == winreq)
			{
				win = 1;
				i = size;
				j = (size - 1);
			}
		}
	}

	return win;
}

int isDiagonal(char** field, int size, int winreq) {

	int win = 0;
	int streak1 = 1;
	int streak2 = 1;
	int streak3 = 1;
	int streak4 = 1;
	int max = size - 1;
	int posuv = size - winreq;

	for (int p = 0; p <= posuv; p++) //p je posuv
	{
		streak1 = 1;
		streak2 = 1;
		streak3 = 1;
		streak4 = 1;
		for (int i = 0; i < (size-1-p); i++) //projizdi vsechny bunky na radku
		{
			if (((field[i + p][i]) == (field[i + p + 1][i + 1])) && ((field[i + p][i]) != ' '))  //posuv na x a porovnani
			{																	// a zaroven neni prvni z nich mezera
				streak1++;
			}
			else streak1 = 1;
			if (((field[i][i + p]) == (field[i + 1][i + p + 1])) && ((field[i][i + p]) != ' '))  //posuv na y a porovnani
			{																	// a zaroven neni prvni z nich mezera
				streak2++;
			}
			else streak2 = 1;
			if (((field[i+p][max-i]) == (field[i+1+p][max-i-1])) && ((field[i+p][max-i]) != ' '))  //posuv na x a porovnani
			{																	// a zaroven neni prvni z nich mezera
				streak3++;
			}
			else streak3 = 1;
			if (((field[i][max - i+p]) == (field[i + 1][max - i - 1+p])) && ((field[i][max-i+p]) != ' '))  //posuv na y a porovnani
			{																	// a zaroven neni prvni z nich mezera
				streak4++;
			}
			else streak4 = 1;

			if (streak1 == winreq || streak2 == winreq || streak3 == winreq || streak4 == winreq)
			{
				win = 1;
				i = size-1-p;
				p = posuv+1;
			}
		}
	}

	return win;
}

int isDraw(char** field, int size) {

	int citac = 0;

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (field[i][j] == ' ') citac++;
		}
	}

	if(citac == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void game(char* buf, char* ex3)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
	WORD saved_attributes;

	/* Save current attributes */
	GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
	saved_attributes = consoleInfo.wAttributes;

	int x;
	int y;
	int error = 0;
	int win = 0;
	int winreq = 0;
	int draw = 0;

	int size;
	char** field;
	char hrac1[15];
	char hrac2[100];
	int vyhry1 = 0;
	int vyhry2 = 0;
	//char cmd = NULL;
	char tmp[100];
	char buffer[10];
	char currentPlayer = 'X';

	if (ex3 != "") {
		strcpy(tmp, ex3);
		strcpy(hrac2, buf);
	}
	else 
		strcpy(hrac2, "hrac2");
		


	//printf("%p\n", &currentPlayer); //00000000005ffbb5
	memcpy(buffer, tmp, sizeof(tmp));
	

	printf("Zadej jmeno hrace 1: ");
	scanf_s("%14s", &hrac1, 15);
	printf(hrac1);
	while (getchar() != '\n');
	vyhry1 = playerCheck("zaznam.txt", hrac1, ",\n");
	printf("\nJmeno hrace 2: ");
	
	printf(hrac2);
	//while (getchar() != '\n');
	vyhry2 = playerCheck("zaznam.txt", hrac2, ",\n");

	do
	{
		//system("cls");
		printf("\nVyberte velikost hraciho pole (3x3 = 3, 5x5 = 5, 10x10 = 10)\t");
		scanf("%ld", &size);
		while (getchar() != '\n');
		//getchar();

	} while (size != 3 && size != 5 && size != 10);
	
	//if (ex3 != NULL)
	//	printf(tmp);
	//printf(buffer);

	printf("\n");
	field = createField(size);

	field = fillField(field, size);

	field = printField(field, size);

	do {
		error = 0;

		if (currentPlayer == 'X')
		{
			printf("Hraje hrac %s s %c:\n", hrac1, currentPlayer);
		}
		else printf("Hraje hrac %s s %c:\n", hrac2, currentPlayer);

		printf("\nZadavejte souradnice");
		SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
		printf(" x");
		SetConsoleTextAttribute(hConsole, saved_attributes);
		printf(" a");
		SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN);
		printf(" y:\n");
		SetConsoleTextAttribute(hConsole, saved_attributes);

		scanf_s("%d", &y);
		//while(getchar() != '\n');
		getchar();
		scanf_s("%d", &x);
		while(getchar() != '\n');
		getchar();

		system("cls");

		error = playerMove(field, size, x, y, currentPlayer, error);

		switch (size)
		{
		case 3:
			winreq = 3;
			printf("Potreba na vyhru: %d\n", winreq);
			break;
		case 5:
			winreq = 4;
			printf("Potreba na vyhru: %d\n", winreq);
			break;
		case 10:
			winreq = 5;
			printf("Potreba na vyhru: %d\n", winreq);
			break;
		}

		field = printField(field, size);

		win = isRows(field, size, winreq);
		win = win + isCols(field, size, winreq);
		win = win + isDiagonal(field, size, winreq);

		if(win == 0)
		{
			draw = isDraw(field, size);
		}

		if (!error)
		{
			currentPlayer = switchPlayer(currentPlayer);
		}

	} while (win == 0 && draw == 0);

	currentPlayer = switchPlayer(currentPlayer);

	if(draw ==1)
	{
		printf("\nRemiza!\n");
	}
	else
	{

		if (currentPlayer == 'X')
		{
			printf("\nVyherce je hrac %s s %c.\n", hrac1, currentPlayer);
			vyhry1 = vyhry1 + 1;
		}
		else
		{
			printf("\nVyherce je hrac %s s %c.\n", hrac2, currentPlayer);
			vyhry2 = vyhry2 + 1;
		}
	}
	writePlayer("zaznam.txt", "help1.txt", hrac1, hrac2, vyhry1, vyhry2);
	printf("\nPro navrat do menu stisknete ENTER.\n");
	getchar();
}

void leaderBoard(const char* file, const char* delice)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
	WORD saved_attributes;

	/* Save current attributes */
	GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
	saved_attributes = consoleInfo.wAttributes;

	FILE* f;
	char buffer[30]; // bude slouzit pro ukladani radku z textaku
	char* token, * next_token;
	int citac = 0;
	char actname[SIZE];
	int actwin;
	int opak = 0;

	fopen_s(&f, file, "r");

	while(!feof(f))
	{
		if (fscanf_s(f, "%s", buffer, 30) > 0)
		{
			token = strtok_s(buffer, delice, &next_token);
			while (token != NULL)
			{
				citac++;
				if ((citac % 2) == 1)
				{
					strcpy_s(actname, token);
				}
				else
				{
					actwin = atoi(token);
					addGame(actname, actwin, &prvni);
				}
				token = strtok_s(NULL, delice, &next_token);
			}
		}
	}
	fclose(f);

	struct game* actGame = prvni; // ukazatel na aktualni hru

	printf("LEADERBOARD\n\n\tPoradi:\t\tHrac:\t\tVyhry:\n");

		while(actGame && opak < 10) // prochazeni seznamu do deseti prvnich
		{
			printf("%10d.\t%15s:%10d\n",(opak+1), actGame->name, actGame->wins); // tisk radku
			actGame = actGame->dalsi; // posun na dalsi hru
			opak++;
		}

		printf("\nPro navrat do menu stisknete ENTER.\n");
		getchar();

		delGame(&prvni);
}

int main(int argc, char *argv[])
{
	int  cmd;
	int x = 0;
	char buf[100];
	char buf2[15] = "hrac2";
	char ex3[100] = "";

	do
	{
		system("cls");		
		printf("0: HRA     ");
		printf("1: LEADERBOARD     ");
		printf("2: KONEC\n\n");
		
		if(argv[1] != NULL){ 
			scanf("%d", &x);
			while (getchar() != '\n');
		}

		scanf("%d", &cmd);			
		cmd += 1;
		printf("%d", cmd);
		while (getchar() != '\n');
		
		switch (cmd)
		{
		case 1:
			system("cls");
			if(argv[1] != NULL){
				strcpy(buf, argv[1]);
				if (x != 0)
					strcpy(ex3, argv[1]);
				//printf(buf2);
				game(buf, ex3);
			}
			else{
				//strcpy(ex3, argv[1]);
				game(buf2, ex3);
			}
			break;
		case 2:
			system("cls");
			leaderBoard("zaznam.txt",",\n");
			break;
		}
	} while (cmd != 3);     // koncime az pri Q
	return 0;
}

