package org.but.feec.booking.data;

import javafx.event.ActionEvent;
import org.but.feec.booking.api.*;
import org.but.feec.booking.config.DataSourceConfig;
import org.but.feec.booking.exceptions.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class HotelRepository {

    private static final Logger logger = LoggerFactory.getLogger(HotelRepository.class);

    public PersonAuthView findPersonByEmail(String user_login) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT user_login, user_password " +
                             " FROM booking.users u" +
                             " WHERE u.user_login = ?")
        ) {
            preparedStatement.setString(1, user_login);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find user failed.", e);
        }
        return null;
    }

    private PersonAuthView mapToPersonAuth(ResultSet rs) throws SQLException {
        PersonAuthView user = new PersonAuthView();
        user.setUsername(rs.getString("user_login"));
        user.setUser_password(rs.getString("user_password"));
        return user;
    }


    public HotelDetailView findHotelDetailedView(Integer hotel_id) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT hotel_name,country,city, street,zip_code, email_address," +
                             "phone_number,rating_star,floor,type,price" +
                             " FROM booking.hotel h" +
                             " LEFT JOIN booking.address a ON h.address_id = a.adress_id" +
                             " LEFT JOIN booking.contact c ON h.contact_id = c.contact_id" +
                             " LEFT JOIN booking.rating r ON h.rating_id = r.rating_id" +
                             " LEFT JOIN  booking.rooms ro ON h.hotel_id = ro.hotel_id" +
                             " LEFT JOIN  booking.room_type rt ON ro.room_type_id = rt.room_type_id" +
                             " WHERE h.hotel_id = ?")

        ) {
            preparedStatement.setInt(1, hotel_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToHotelDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Loading hotel detail view failed", e);
        }
        return null;
    }

    private HotelDetailView mapToHotelDetailView(ResultSet rs) throws SQLException {
        HotelDetailView hotelDetailView = new HotelDetailView();
        hotelDetailView.setHotel_name(rs.getString("hotel_name"));
        hotelDetailView.setCountry(rs.getString("country"));
        hotelDetailView.setCity(rs.getString("city"));
        hotelDetailView.setStreet(rs.getString("street"));
        hotelDetailView.setZip_code(rs.getInt("zip_code"));
        hotelDetailView.setEmail_address(rs.getString("email_address"));
        hotelDetailView.setPhone_number(rs.getInt("phone_number"));
        hotelDetailView.setRating_star(rs.getInt("rating_star"));
        hotelDetailView.setFloor(rs.getInt("floor"));
        hotelDetailView.settype(rs.getString("type"));
        hotelDetailView.setPrice(rs.getDouble("price"));
        return hotelDetailView;
    }

    public List<HotelBasicView> getHotelBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT hotel_id, hotel_name, country, city, rating_star, email_address, phone_number" +
                             " FROM booking.hotel h" +
                             " LEFT JOIN booking.address a ON h.address_id = a.adress_id" +
                             " LEFT JOIN booking.contact c ON h.contact_id = c.contact_id" +
                             " LEFT JOIN booking.rating r ON h.rating_id = r.rating_id");

             ResultSet resultSet = preparedStatement.executeQuery()) {
            List<HotelBasicView> HotelBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                HotelBasicViews.add(mapToHotelBasicView(resultSet));
            }
            return HotelBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("Loading hotel basic view failed", e);
        }
    }

    private HotelBasicView mapToHotelBasicView(ResultSet rs) throws SQLException {
        HotelBasicView hotelBasicView = new HotelBasicView();
        hotelBasicView.setHotel_id(rs.getInt("hotel_id"));
        hotelBasicView.setHotel_name(rs.getString("hotel_name"));
        hotelBasicView.setCountry(rs.getString("country"));
        hotelBasicView.setCity(rs.getString("city"));
        hotelBasicView.setRating_star(rs.getInt("rating_star"));
        hotelBasicView.setEmail_address(rs.getString("email_address"));
        hotelBasicView.setPhone_number(rs.getInt("phone_number"));
        return hotelBasicView;
    }

    public static void updateQuery(ActionEvent event, String table, int id, String column, String value) {
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        String query = "UPDATE public." + table + " SET " + column + " = " + value + " WHERE " + table + "_id = " + id + ";";
        try (Connection connection = DataSourceConfig.getConnection()) {
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            logger.error("Cannot execute the query.", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void insertQuery(ActionEvent event, String query) {
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;

        try (Connection connection = DataSourceConfig.getConnection()) {
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            logger.error("Cannot execute the query.", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void deleteQuery(ActionEvent event, String table, int id) {
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        String query = "DELETE FROM booking." + table + " WHERE " + table + "_id = " + id + ";";

        try (Connection connection = DataSourceConfig.getConnection()) {
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            logger.error("Cannot execute the query.", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}