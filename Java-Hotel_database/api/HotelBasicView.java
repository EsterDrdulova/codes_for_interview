package org.but.feec.booking.api;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class HotelBasicView {

    private IntegerProperty hotel_id = new SimpleIntegerProperty();
    private StringProperty hotel_name = new SimpleStringProperty();
    private StringProperty country = new SimpleStringProperty();
    private StringProperty city = new SimpleStringProperty();
    private IntegerProperty rating_star = new SimpleIntegerProperty();
    private StringProperty email_address = new SimpleStringProperty();
    private IntegerProperty phone_number = new SimpleIntegerProperty();


    public void setRating_star(Integer rating_star) {
        this.rating_star.set(rating_star);
    }

    public Integer getRating_star() {
        return rating_star.get();
    }

    public Integer getHotel_id() {
        return hotel_idProperty().get();
    }

    public void setHotel_id(Integer hotel_id) {
        this.hotel_idProperty().setValue(hotel_id);
    }

    public String getHotel_name() {
        return hotel_nameProperty().get();
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_nameProperty().setValue(hotel_name);
    }

    public String getCountry() {
        return countryProperty().get();
    }

    public void setCountry(String country) {
        this.countryProperty().setValue(country);
    }

    public String getCity() {
        return cityProperty().get();
    }

    public void setCity(String city) {
        this.cityProperty().setValue(city);
    }

    public String getEmail_address() {
        return email_addressProperty().get();
    }

    public void setEmail_address(String email_address) {
        this.email_addressProperty().setValue(email_address);
    }

    public IntegerProperty hotel_idProperty() {
        return hotel_id;
    }

    public StringProperty hotel_nameProperty() {
        return hotel_name;
    }

    public StringProperty countryProperty() {
        return country;
    }

    public StringProperty cityProperty() {
        return city;
    }

    public StringProperty email_addressProperty() {
        return email_address;
    }

    public Integer getPhone_number() {
        return phone_number.get();
    }

    public void setPhone_number(Integer phone_number) {
        this.phone_number.set(phone_number);
    }
}
