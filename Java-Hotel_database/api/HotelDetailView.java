package org.but.feec.booking.api;

import javafx.beans.property.*;

public class HotelDetailView {

    private IntegerProperty hotel_id = new SimpleIntegerProperty();
    private StringProperty hotel_name = new SimpleStringProperty();
    private StringProperty country = new SimpleStringProperty();
    private StringProperty city = new SimpleStringProperty();
    private StringProperty street = new SimpleStringProperty();
    private IntegerProperty zip_code = new SimpleIntegerProperty();
    private StringProperty email_address = new SimpleStringProperty();
    private IntegerProperty phone_number = new SimpleIntegerProperty();
    private IntegerProperty rating_star = new SimpleIntegerProperty();
    private IntegerProperty floor = new SimpleIntegerProperty();
    private StringProperty type = new SimpleStringProperty();
    private DoubleProperty price = new SimpleDoubleProperty();

    public String gettype() {return type.get();}

    public void settype(String type) {this.type.set(type);}

    public Integer getFloor() {
        return floor.get();
    }

    public void setFloor(Integer floor) {
        this.floor.set(floor);
    }

    public Integer getZip_code() {
        return zip_code.get();
    }

    public void setZip_code(Integer zip_code) {
        this.zip_code.set(zip_code);
    }

    public String getEmail_address() {
        return email_address.get();
    }

    public void setEmail_address(String email_address) {
        this.email_address.set(email_address);
    }

    public Integer getRating_star() {
        return rating_star.get();
    }

    public void setRating_star(Integer rating_star) {
        this.rating_star.set(rating_star);
    }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }


    public Integer getHotel_id() {
        return hotel_idProperty().get();
    }

    public void setHotel_id(Integer hotel_id) {
        this.hotel_idProperty().setValue(hotel_id);
    }


    public String getHotel_name() {
        return hotel_nameProperty().get();
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_nameProperty().setValue(hotel_name);
    }

    public String getCountry() {
        return countryProperty().get();
    }

    public void setCountry(String country) {
        this.countryProperty().setValue(country);
    }

    public Integer getPhone_number() {
        return phone_numberProperty().get();
    }

    public void setPhone_number(Integer phone_number) {
        this.phone_numberProperty().setValue(phone_number);
    }

    public String getCity() {
        return cityProperty().get();
    }

    public void setCity(String city) {
        this.cityProperty().setValue(city);
    }


    public String getStreet() {
        return streetProperty().get();
    }

    public void setStreet(String street) {
        this.streetProperty().setValue(street);
    }




    public IntegerProperty hotel_idProperty() {
        return hotel_id;
    }

    public StringProperty hotel_nameProperty() {
        return hotel_name;
    }

    public StringProperty countryProperty() {
        return country;
    }

    public IntegerProperty phone_numberProperty() {
        return phone_number;
    }

    public StringProperty cityProperty() {
        return city;
    }

    public StringProperty streetProperty() {
        return street;
    }
}
