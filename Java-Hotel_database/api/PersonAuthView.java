package org.but.feec.booking.api;

public class PersonAuthView {

    private String user_login;
    private String user_password;

    public String getUsername() {
        return user_login;
    }

    public void setUsername(String userName) {
        this.user_login = userName;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String Password) {
        this.user_password = Password;
    }

    @Override
    public String toString() {
        return "PersonAuthView{" +
                "user_login='" + user_login + '\'' +
                ", user_password='" + user_password + '\'' +
                '}';
    }
}
