package org.but.feec.booking.api;


import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DummyView {

    private StringProperty col_1 = new SimpleStringProperty();
    private DoubleProperty col_2 = new SimpleDoubleProperty();
    private StringProperty col_3 = new SimpleStringProperty();

    public void setcol1(String column_1) {
        this.col_1.set(column_1);
    }

    public String getcol1() {
        return col_1.get();
    }

    public void setcol2(Double column_2) {this.col_2.set(column_2);}

    public Double getcol2() {
        return col_2.get();
    }

    public void setcol3(String column_3) {
        this.col_3.set(column_3);
    }

    public String getcol3() {
        return col_3.get();
    }

}
