package org.but.feec.booking.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DataSourceConfig {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);
    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    private static final String APPLICATION_PROPERTIES = "application.properties";

    static {
        try (InputStream resourceStream = DataSourceConfig.class.getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES)) {
            Properties properties = new Properties();

            properties.load(resourceStream);

            config.setJdbcUrl(properties.getProperty("datasource.url"));
            config.setUsername(properties.getProperty("datasource.user_login"));
            config.setPassword(properties.getProperty("datasource.user_password"));
            ds = new HikariDataSource(config);
        } catch (IOException | NullPointerException | IllegalArgumentException e) {
            logger.error("Configuration of the datasource was not successful.", e);
        } catch (Exception e) {
            logger.error("Could not connect to the database.", e);
        }
    }

    private DataSourceConfig() {
    }

    public static DataSource getDataSource() {
        return ds;
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public static void changeScene(ActionEvent event, String fxmlFile) {
        Parent root = null;
        FXMLLoader loader = null;
        try {
            loader = new FXMLLoader(DataSourceConfig.class.getResource(fxmlFile));
            root = loader.load();
        } catch (IOException e) {
            logger.error("Cannot change the scene.", e);
        }
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }

}
