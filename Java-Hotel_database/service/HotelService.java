package org.but.feec.booking.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.booking.api.HotelBasicView;
import org.but.feec.booking.api.HotelDetailView;
import org.but.feec.booking.data.HotelRepository;

import java.util.List;

public class HotelService {
    private HotelRepository hotelRepository;

    public HotelService(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    public HotelDetailView geHotelDetailView(Integer id) {
        return hotelRepository.findHotelDetailedView(id);
    }

    public List<HotelBasicView> getHotelBasicView() {
        return hotelRepository.getHotelBasicView();
    }

   /* public void createPerson(PersonCreateView personCreateView) {
        String originalPassword = personCreateView.getPwd();
        String hashedPassword = hashPassword(originalPassword);
        personCreateView.setPwd(hashedPassword);

        hotelRepository.createPerson(personCreateView);
    }

    public void editPerson(PersonEditView personEditView) {
        hotelRepository.editPerson(personEditView);
    }*/

    private String hashPassword(String password) {
        return BCrypt.withDefaults().hashToString(12, password.toCharArray());
    }
}
