package org.but.feec.booking.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.booking.api.PersonAuthView;
import org.but.feec.booking.config.DataSourceConfig;
import org.but.feec.booking.data.HotelRepository;
import org.but.feec.booking.exceptions.ResourceNotFoundException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class AuthService {

    private HotelRepository hotelRepository;

    public AuthService(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    private PersonAuthView findPersonByUsername(String username) {
        return hotelRepository.findPersonByEmail(username);
    }

    public boolean authenticate(String username, String password) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }

        PersonAuthView personAuthView = findPersonByUsername(username);
        if (personAuthView == null) {
            throw new ResourceNotFoundException("Provided username is not found.");
        }

        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), personAuthView.getUser_password());
       System.out.print("verified: " + result.verified);
        return result.verified;

    }

    public static void main(String[] args) {
        String pass = "batman";
        String bcryptString = BCrypt.withDefaults().hashToString(6, pass.toCharArray());
        System.out.println(bcryptString);
        BCrypt.Result result = BCrypt.verifyer().verify(pass.toCharArray(), bcryptString.toCharArray());
        System.out.println(result);
        String updatePassOnHash = "UPDATE booking.users SET user_password =" + bcryptString  + "WHERE user_id < 100" + ";";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement statement = connection.prepareStatement(updatePassOnHash)){
                statement.setString(1, bcryptString);

        }catch (SQLException e) { // handle exception}


    }

    }
}
