package org.but.feec.booking.controller;


import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.but.feec.booking.config.DataSourceConfig;
import org.but.feec.booking.data.HotelRepository;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;


public class UpdateController implements Initializable  {
    @FXML
    private ChoiceBox ChooseTable;
    @FXML
    private ChoiceBox ChooseColumn;
    @FXML
    private TextField setId;
    @FXML
    private TextField updatedValue;
    @FXML
    private Button updateButton;
    @FXML
    private Button Xbutton;

    public String option;
    @Override
    public void initialize (URL url, ResourceBundle resourceBundle) {
        ChooseTable.setItems(FXCollections.observableArrayList(
                "hotel","address","contact","rating","users","room_type",
                "role","rooms","bookings","guests","services","guest_has_service"));

        ChooseTable.setValue("hotel");

        ChooseTable.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                ChooseColumn.setVisible(true);
                option = ((String) ChooseTable.getItems().get((Integer) t1)).toLowerCase(Locale.ROOT);
                if (option.equals("hotel")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("hotel_id", "hotel_name", "address_id", "contact_id", "rating_id"));
                }

                if (option.equals("address")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("adress_id", "country", "city", "street", "number", "zip_code"));
                }

                if (option.equals("contact")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("contact_id", "email_address", "phone_number"));
                }

                if (option.equals("rating")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("rating_id", "rating_star"));
                }

                if (option.equals("users")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("user_id", "hotel_id", "name", "surname", "user_login", "password"));
                }

                if (option.equals("room_type")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("room_type_id", "type", "price"));
                }

                if (option.equals("role")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("role_id", "user_role"));
                }

                if (option.equals("rooms")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("room_id", "number", "floor", "room_type_id", "hotel_id"));
                }

                if (option.equals("bookings")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("booking_id", "date_from", "date_to", "guest_id"));
                }

                if (option.equals("guests")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("guest_id", "name", "surname", "date_of_birth"));
                }

                if (option.equals("services")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("service_id", "service_type"));
                }

                if (option.equals("guest_has_service")) {
                    ChooseColumn.setItems(FXCollections.observableArrayList("service_id", "guest_id"));
                }

            }
        });

    //nastavenie akcie pre tlacitko UPDATE
        updateButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                int ID = Integer.parseInt(setId.getText());
                String table = ((String) ChooseTable.getValue()).toLowerCase();
                String column = ((String) ChooseColumn.getValue());
                String value = updatedValue.getText();
                //System.out.println(table + column + ID + value);
                HotelRepository.updateQuery(event, table, ID, column, value);
                DataSourceConfig.changeScene(event, "MainDialog.fxml");
            }
        });

        //nastavenie akcie pre X tlacitko aby sa aplikacia vypla
        Xbutton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Platform.exit();
            }
        });

        }
    }


