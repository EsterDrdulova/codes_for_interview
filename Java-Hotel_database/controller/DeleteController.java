package org.but.feec.booking.controller;


import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.but.feec.booking.config.DataSourceConfig;
import org.but.feec.booking.data.HotelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;


public class DeleteController implements Initializable {

    private static final Logger logger = LoggerFactory.getLogger(DeleteController.class);

    @FXML
    private ChoiceBox ChooseTable;
    @FXML
    private TextField setId;
    @FXML
    private Button deleteButton;
    @FXML
    private Button Xbutton;

    private static String option;

    public DeleteController() {
    }

    @FXML
    public void initialize (URL url, ResourceBundle resourceBundle) {

        ChooseTable.setItems(FXCollections.observableArrayList(
                "hotel","address","contact","rating","users","room_type",
                "role","rooms","bookings","guests","services","guest_has_service"));
        ChooseTable.setValue("hotel");

        ChooseTable.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            //@Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                option = ((String) ChooseTable.getItems().get((Integer) t1)).toLowerCase(Locale.ROOT);
            }
        });

        deleteButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                int ID = Integer.parseInt(setId.getText());
                HotelRepository.deleteQuery(event, option, ID);
                DataSourceConfig.changeScene(event, "MainDialog.fxml");
            }
        });

        Xbutton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Platform.exit();
            }
        });

        logger.info("DeleteController initialized");

}
}
