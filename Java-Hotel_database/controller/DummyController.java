package org.but.feec.booking.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import org.but.feec.booking.api.DummyView;
import org.but.feec.booking.config.DataSourceConfig;
import org.but.feec.booking.exceptions.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DummyController {
    private static final Logger logger = LoggerFactory.getLogger(DummyController.class);

    @FXML
    private TableColumn<DummyView, String> TableColumn_1;
    @FXML
    private TableColumn<DummyView, Double> TableColumn_2;
    @FXML
    private TableColumn<DummyView, String> TableColumn_3;
    @FXML
    private TextField InjectionTextField;
    @FXML
    public Button runButton;
    @FXML
    public Button refreshButton;
    @FXML
    public Button recreateButton;
    @FXML
    private TableView<DummyView> dummySystemView;



    @FXML
    private void initialize() {

        TableColumn_1.setCellValueFactory(new PropertyValueFactory<DummyView, String>("column_1"));
        TableColumn_2.setCellValueFactory(new PropertyValueFactory<DummyView, Double>("column_2"));
        TableColumn_3.setCellValueFactory(new PropertyValueFactory<DummyView, String>("column_3"));
        logger.info("Dummy Controller initialized");
    }


    private DummyView mapToDummyView(ResultSet rs) throws SQLException {
        DummyView dummyView = new DummyView();
        dummyView.setcol1(rs.getString("column_1"));
        dummyView.setcol2(rs.getDouble("column_2"));
        dummyView.setcol3(rs.getString("column_3"));
        return dummyView;
    }


    public List<DummyView> getDummyView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT column_1, column_2, column_3" +
                             " FROM booking.dummy_table");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            List<DummyView> sqlBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                sqlBasicViews.add(mapToDummyView(resultSet));
            }
            return sqlBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("SQL basic view could not be loaded.", e);
        }
    }


    public void handleSqlCreate(ActionEvent actionEvent) {
        String sql_query = "CREATE TABLE IF NOT EXISTS booking.dummy_table" +
                " (id SERIAL NOT NULL PRIMARY KEY, column_1 VARCHAR NOT NULL, column_2 REAL NOT NULL, column_3 VARCHAR NOT NULL);";
        try (Connection connection = DataSourceConfig.getConnection()){
            connection.createStatement().executeUpdate(sql_query);
            handleSqlInsert();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void handleSqlInsert() throws SQLException {
        String sql_query = "INSERT INTO booking.dummy_table (column_1, column_2, column_3) " +
                "VALUES ('Value1', 1000.0, 'Value1'), ('WP', 1000.0, 'Value2'), ('SUPA', 1500.0, 'SIKRIT'), ('SQL', 2000.0, 'INJECTION');";
        try (Connection connection = DataSourceConfig.getConnection()){
            connection.createStatement().executeUpdate(sql_query);
            tableCreatedConfirmationDialog();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void tableCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Table Created Confirmation");
        alert.setHeaderText("Dummy table was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

    public void handleSqlQuery(ActionEvent actionEvent) {
        String parameter = InjectionTextField.getText();

        String sql_query = "SELECT column_1, column_2, column_3 FROM booking.dummy_table WHERE column_1 = '"+parameter+"';";
        try (Connection connection = DataSourceConfig.getConnection()){

            List<DummyView> dummyViews = new ArrayList();
            ResultSet rs = connection.createStatement().executeQuery(sql_query);

            while (rs.next()) {
                dummyViews.add(mapToDummyView(rs));
            }
            dummySystemView.setItems(FXCollections.observableArrayList(dummyViews));

        } catch (SQLException e) {
            throw new DataAccessException("SQL query could not be performed.", e);
        }
    }

    public void handleRefresh(ActionEvent actionEvent) {
        dummySystemView.setItems(null);
        dummySystemView.refresh();
        dummySystemView.sort();
    }
}
