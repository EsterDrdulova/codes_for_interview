package org.but.feec.booking.controller;


import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.but.feec.booking.data.HotelRepository;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class InsertController implements Initializable {
    @FXML
    private Label InsertLabel;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private Label label4;
    @FXML
    private Label label5;
    @FXML
    private Label label6;
    @FXML
    private ChoiceBox ChooseTable;
    @FXML
    private TextField textField1;
    @FXML
    private TextField textField2;
    @FXML
    private TextField textField3;
    @FXML
    private TextField textField4;
    @FXML
    private TextField textField5;
    @FXML
    private TextField textField6;
    @FXML
    private Button insertButton;
    @FXML
    private Button Xbutton;

    int a;
    @Override
    public void initialize (URL url, ResourceBundle resourceBundle) {

        ChooseTable.setItems(FXCollections.observableArrayList(
                "hotel","address","contact","rating","users","room_type",
                "role","rooms","bookings","guests","services","guest_has_service"));

        ChooseTable.setValue("Choose the table!");

        ChooseTable.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                String choice = (String) ChooseTable.getItems().get((Integer) t1);
                switch (choice) {
                    case "hotel":
                        label1.setText("hotel_id");
                        label2.setText("hotel_name");
                        label3.setText("address_id");
                        label4.setText("contact_id");
                        label5.setText("rating_id");
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=5;
                        break;

                    case "address":
                        label1.setText("adress_id");
                        label2.setText("country");
                        label3.setText("city");
                        label4.setText("street");
                        label5.setText("number");
                        label6.setText("zip_code");
                        a=6;
                        break;

                    case "contact":
                        label1.setText("contact_id");
                        label2.setText("email_address");
                        label3.setText("phone_number");
                        label4.setVisible(false);
                        textField4.setVisible(false);
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=3;
                        break;

                    case "rating":
                        label1.setText("rating_id");
                        label2.setText("rating_star");
                        label3.setVisible(false);
                        textField3.setVisible(false);
                        label4.setVisible(false);
                        textField4.setVisible(false);
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=2;
                        break;

                    case "users":
                        label1.setText("user_id");
                        label2.setText("hotel_id");
                        label3.setText("name");
                        label4.setText("surname");
                        label5.setText("user_login");
                        label6.setText("password");
                        a=6;
                        break;


                    case "role":
                        label1.setText("role_id");
                        label2.setText("user_role");
                        label3.setVisible(false);
                        textField3.setVisible(false);
                        label4.setVisible(false);
                        textField4.setVisible(false);
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=2;
                        break;

                    case "rooms":
                        label1.setText("room_id");
                        label2.setText("number");
                        label3.setText("floor");
                        label4.setText("room_type_id");
                        label5.setText("hotel_id");
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=5;
                        break;

                    case "room_type":
                        label1.setText("room_type_id");
                        label2.setText("type");
                        label3.setText("price");
                        label4.setVisible(false);
                        textField4.setVisible(false);
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=3;
                        break;

                    case "bookings":
                        label1.setText("booking_id");
                        label2.setText("date_from");
                        label3.setText("date_to");
                        label4.setText("guest_id");
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=4;
                        break;

                    case "guests":
                        label1.setText("guest_id");
                        label2.setText("name");
                        label3.setText("surname");
                        label4.setText("date_of_birth");
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=4;
                        break;

                    case "services":
                        label1.setText("service_id");
                        label2.setText("service_type");
                        label3.setVisible(false);
                        textField3.setVisible(false);
                        label4.setVisible(false);
                        textField4.setVisible(false);
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=2;
                        break;

                    case "guest_has_service":
                        label1.setText("service_id");
                        label2.setText("guest_id");
                        label3.setVisible(false);
                        textField3.setVisible(false);
                        label4.setVisible(false);
                        textField4.setVisible(false);
                        label5.setVisible(false);
                        textField5.setVisible(false);
                        label6.setVisible(false);
                        textField6.setVisible(false);
                        a=2;
                        break;

                }
            }
            });

        insertButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                String table = ((String) ChooseTable.getValue()).toLowerCase();
                String query = null;
                switch (a) {
                    case 2:
                        query = "INSERT INTO booking." + table + " (" + label1.getText().toLowerCase(Locale.ROOT) + ", "
                                + label2.getText().toLowerCase(Locale.ROOT)  + ") VALUES ("
                                + textField1.getText() + ", " + textField2.getText() + ");";
                        break;

                    case 3:
                        query = "INSERT INTO booking." + table + " (" + label1.getText().toLowerCase(Locale.ROOT) + ", "
                                + label2.getText().toLowerCase(Locale.ROOT) + ", " + label3.getText().toLowerCase(Locale.ROOT) + ") VALUES ("
                                + textField1.getText() + ", " + textField2.getText() + ", " + textField3.getText() + ");";
                        break;

                    case 4:
                        query = "INSERT INTO booking." + table + " (" + label1.getText().toLowerCase(Locale.ROOT) + ", "
                                + label2.getText().toLowerCase(Locale.ROOT) + ", "   + label3.getText().toLowerCase(Locale.ROOT) + ", "
                                + label4.getText().toLowerCase(Locale.ROOT) + ") VALUES (" + textField1.getText() + ", "
                                + textField2.getText() + ", " + textField3.getText() + ", " + textField4.getText() + ");";
                        break;

                    case 5:
                        query = "INSERT INTO booking." + table + " (" + label1.getText().toLowerCase(Locale.ROOT) + ", "
                                + label2.getText().toLowerCase(Locale.ROOT) + ", "   + label3.getText().toLowerCase(Locale.ROOT) + ", "
                                + label4.getText().toLowerCase(Locale.ROOT) + ", " + label5.getText().toLowerCase(Locale.ROOT) + ")"
                                + " VALUES (" + textField1.getText() + ", " + textField2.getText() + ", " + textField3.getText()
                                + ", " + textField4.getText() + ", " + textField5.getText() + ");";
                        break;

                    case 6:
                        query = "INSERT INTO booking." + table + " (" + label1.getText().toLowerCase(Locale.ROOT) + ", "
                                + label2.getText().toLowerCase(Locale.ROOT) + ", "   + label3.getText().toLowerCase(Locale.ROOT) + ", "
                                + label4.getText().toLowerCase(Locale.ROOT) + ", "  + label5.getText().toLowerCase(Locale.ROOT) + ", "
                                + label6.getText().toLowerCase(Locale.ROOT) + ")"
                                + " VALUES (" + textField1.getText() + ", " + textField2.getText() + ", " + textField3.getText()
                                + ", " + textField4.getText() + ", " + textField5.getText() + ", " + textField6.getText() + ");";
                        break;
                }
                HotelRepository.insertQuery(event, query);
                //DataSourceConfig.changeScene(event, "MainDialog.fxml");
            }
        });

        Xbutton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Platform.exit();
            }
        });

    }
}