package org.but.feec.booking.controller;


import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.but.feec.booking.config.DataSourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.Random;
import java.util.ResourceBundle;


public class MainDialogController  {

    public static boolean canFilter = false;
    public static boolean canInsert = false;
    public static boolean canUpdate = false;
    public static boolean canDelete = false;

    private static final Logger logger = LoggerFactory.getLogger(MainDialogController.class);
    private static TableColumn column;


    @FXML
    private Button insertButton;
    @FXML
    private Button updateButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button viewAllButton;
    @FXML
    private Button filterButton;
    @FXML
    private TextField filterField;
    @FXML
    private TableView tableView;
    @FXML
    private Button Xbutton;

    public MainDialogController() {
    }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {

        if (!canFilter) {
            filterButton.setVisible(false);
        }

        if (!canInsert) {
            insertButton.setVisible(false);
        }

        if (!canUpdate) {
            updateButton.setVisible(false);
        }

        if (!canDelete) {
            deleteButton.setVisible(false);
        }


        filterField.setVisible(true);

        viewAllButton.setText("VIEW ALL HOTELS");

        viewAllButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @FXML
            public void handle(javafx.event.ActionEvent event) {
                for (int i = 0; i < tableView.getItems().size(); i++) {
                    tableView.getItems().clear();
                }
                for (int i = 0; i < tableView.getColumns().size(); i++) {
                    tableView.getColumns().clear();
                }
                filterQuery("SELECT * FROM booking.hotel");
            }
        });

        filterButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @FXML
            public void handle(javafx.event.ActionEvent event) {
                for (int i = 0; i < tableView.getItems().size(); i++) {
                    tableView.getItems().clear();
                }
                for (int i = 0; i < tableView.getColumns().size(); i++) {
                    tableView.getColumns().clear();
                }
                if (!filterField.getText().isEmpty()) {
                    filterQuery(filterField.getText());
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Write a query");
                    alert.show();
                }
            }
        });


        insertButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @FXML
            public void handle(javafx.event.ActionEvent event) {
                Alert notification = new Alert(javafx.scene.control.Alert.AlertType.INFORMATION);
                notification.setContentText("For strings, enter a value into quotation marks. For example: 'Java'.");
                notification.showAndWait();
                notification.setContentText("For booleans, enter true or false. For example: true");
                notification.showAndWait();
                notification.setContentText("For dates, enter a YYYY-MM-DD. For example: 2021-12-24");
                notification.showAndWait();
                DataSourceConfig.changeScene(event, "Insert.fxml");
            }
        });

        updateButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @FXML
            public void handle(javafx.event.ActionEvent event) {
                Alert notification = new Alert(javafx.scene.control.Alert.AlertType.INFORMATION);
                notification.setContentText("For strings, enter a value into quotation marks. For example: 'Java'.");
                notification.showAndWait();
                notification.setContentText("For booleans, enter true or false. For example: true");
                notification.showAndWait();
                notification.setContentText("For dates, enter a YYYY-MM-DD. For example: 2021-12-24");
                notification.showAndWait();
                DataSourceConfig.changeScene(event, "Update.fxml");
            }
        });

        deleteButton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @FXML
            public void handle(javafx.event.ActionEvent event) {
                DataSourceConfig.changeScene(event, "Delete.fxml");
            }
        });


        Xbutton.setOnAction(new javafx.event.EventHandler<javafx.event.ActionEvent>() {
            @FXML
            public void handle(javafx.event.ActionEvent event) {
                Platform.exit();
            }
        });


    }


    public void filterQuery(String query) {
        PreparedStatement preparedStatement = null;
        ObservableList<ObservableList> data = null;
        ResultSet resultSet = null;

        try (Connection connection = DataSourceConfig.getConnection()){
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            data = FXCollections.observableArrayList();
            for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                final int j = i;
                column = new TableColumn(resultSet.getMetaData().getColumnName(i + 1));
                column.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });
                tableView.getColumns().addAll(column);
            }
            while (resultSet.next()) {
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    row.add(resultSet.getString(i));
                }
                data.add(row);
            }
            tableView.setItems(data);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Invalid SQL query, try again");
            alert.show();
            logger.error("Try again", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
